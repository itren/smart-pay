package com.founder.config;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

@Configuration
public class DataSourceConfig {

    @Primary    //配置该数据源为主数据源
    @Bean(name = "mysqlDataSource")
    @Qualifier(value = "mysqlDataSource")
    @ConfigurationProperties(prefix = "spring.datasource.mysql")
    public DataSource mysqlDataSource(){
        return DataSourceBuilder.create().build();
    }

    @Bean(name = "mssqlDataSource")
    @Qualifier(value = "mssqlDataSource")  //spring装配bean的唯一标识
    @ConfigurationProperties(prefix = "spring.datasource.mssql")   //application.properties配置文件中该数据源的配置前缀
    public DataSource mssqlDataSource(){
        return DataSourceBuilder.create().build();
    }

}
